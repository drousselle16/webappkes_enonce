
package entites;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Client implements Serializable {
    
    
    //<editor-fold defaultstate="collapsed" desc="Identifiant Clé primaire">
    
    @Id
    private Long     numero;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Attributs informationnels">
    
    private String   nom;
    private String   adresse;
    private int      distanceKM;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Attrinuts navigationnels">
    
    @OneToMany(mappedBy = "leClient")
    private List<Contrat> lesContrats= new LinkedList<Contrat>();
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et setters">
    
    public int getDistanceKM() {
        return distanceKM;
    }
    
    public void setDistanceKM(int distanceKM) {
        this.distanceKM = distanceKM;
    }
    
    public Long getNumero() {
        return numero;
    }
    
    public void setNumero(Long numero) {
        this.numero = numero;
    }
    
     public List<Contrat> getLesContrats() {
        return lesContrats;
    }

    public void setLesContrats(List<Contrat> lesContrats) {
        this.lesContrats = lesContrats;
    }
    
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
   
    
    
    
    //</editor-fold>

}
