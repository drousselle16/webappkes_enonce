
package entites;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import static persistance.Persistance.getEm;
import utilitaires.UtilDate;
import static utilitaires.UtilDate.annee;
import static utilitaires.UtilDate.anneeCourante;
import static utilitaires.UtilDate.nombreAnneesEcouleesDepuis;

@Entity
public class Technicien implements Serializable {
    
    //<editor-fold defaultstate="collapsed" desc="Identifiant Clé primaire">
    
    @Id
    private Long   numero;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Attributs informationnels">
    
    private String nom;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateEmbauche;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Attributs navigationnels">
    
    @OneToMany(mappedBy = "leTechnicien")
    private List<Intervention> lesInterventions= new LinkedList<Intervention>();
    
    @ManyToOne
    private Grade leGrade;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="DAL">
    public static Technicien getLeTechnicien(Long numtech){
    
        return persistance.Persistance.getEm().find(Technicien.class, numtech);
    }
    
     public static Technicien getLeTechnicienNumero(Long numero){
     
         return getEm().find(Technicien.class, numero);
     }
     
     public static List<Technicien> getTousLesTechniciens(){
     
         return getEm().createQuery("Select t from Technicien t").getResultList();
     }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="BLL Méthodes d'instance">
    
     public Float coutHoraireTechnicien(){
        
       float cout;
       float CoutHoraireGrade = leGrade.getTauxHoraire();
       int anc = UtilDate.nombreAnneesEcouleesDepuis(dateEmbauche);
       float majo=1f;
       
       if(anc>=5 && anc <11){
           majo=1.05f;
       }
       else{
           if(anc>=11 && anc <16){
               majo=1.08f;
           }
           else if (anc>=16){
               majo=1.12f;
           }
       
       }
        cout=CoutHoraireGrade*majo;
        
        return cout;
    }
    
    public float getCoeffMajo(){
        int anc=getAnciennete();
        
        float coeff=1f;
        
        if (anc>=16) coeff = 1.12f;
        else if (anc >=11) coeff = 1.08f;
        else if (anc >=4) coeff = 1.05f;
        
        return coeff;
        
    }
    public int getAnciennete(){return nombreAnneesEcouleesDepuis(dateEmbauche);}
    public static int nombreAnneesEcouleesDepuis(Date date){ return anneeCourante()-annee(date);}
    
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public Grade getLeGrade() {
        return leGrade;
    }
    
    public void setLeGrade(Grade leGrade) {
        this.leGrade = leGrade;
    }
    
    public Date getDateEmbauche() {
        return dateEmbauche;
    }
    
    public void setDateEmbauche(Date dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public List<Intervention> getLesInterventions() {
        return lesInterventions;
    }

    public void setLesInterventions(List<Intervention> lesInterventions) {
        this.lesInterventions = lesInterventions;
    }
    
    public Long getNumero() {
        return numero;
    }
    
    public void setNumero(Long numero) {
        this.numero = numero;
    }
    
     public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    //</editor-fold>
   
}
