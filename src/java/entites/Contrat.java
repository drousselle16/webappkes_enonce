
package entites;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import utilitaires.UtilDate;
import static utilitaires.UtilDate.*;


@Entity
public class Contrat implements Serializable {
    
    
    //<editor-fold defaultstate="collapsed" desc="Identifiant Clé primaire">
    
    @Id private Long numero;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Attributs informationnels">
    
    private Float    montantContrat;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private  Date dateCont;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Attributs navigationnels">
    
    @OneToMany(mappedBy = "leContrat")
    private List<Intervention> lesInterventions= new LinkedList<Intervention>();
    
    @ManyToOne
    private Client leClient;
    //</editor-fold>
 
    //<editor-fold defaultstate="collapsed" desc="DAL">
    
    
     public static Contrat        getLeContrat(Long numcont){
    
        return persistance.Persistance.getEm().find(Contrat.class, numcont);
    }
    
     public static List<Contrat> getTousLesContrats(){
        
        return persistance.Persistance.getEm().createQuery("Select c From Contrat c").getResultList();
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="BLL méthodes d'instance">
    
    public Float   ecartAnnee(){
        
        return montantContrat-coutTotalContrat();
    }
    
    public Float coutTotalContrat() {
        
        float cumul=0f;
        for(Intervention interv: lesInterventions){if(UtilDate.annee(interv.getDateInterv())==UtilDate.anneeCourante()){cumul=cumul+interv.coutInterv();}}
       
        return cumul;
    }
    
    public List<Intervention> getInterventionsAnneeCourante(){
    
     List<Intervention> lesIntervs= new LinkedList();
     
     for (Intervention interv : this.lesInterventions){ 
         
          if ( annee(interv.getDateInterv())== anneeCourante()) lesIntervs.add(interv);
             
     }
     
     return lesIntervs;
        
      
    }
    
    
    //</editor-fold> 
    
    //<editor-fold defaultstate="collapsed" desc="BLL méthodes de classe">
    
    
    public static Float  ecartTotalAnnee(){
        
        float cumul=0f;
        for(Contrat cont:getTousLesContrats()){
            cumul += cont.ecartAnnee();
        }
        
        return cumul;
    }
    
    
    public static List<Contrat>      getLesContratsDeficitaires(Float seuil){
        
        
        return null;
    }
    
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">

    
    public Float getMontantContrat() {
        return montantContrat;
    }

    public void setMontantContrat(Float montantContrat) {
        this.montantContrat = montantContrat;
    }
   
    public Client getLeClient() {
        return leClient;
    }
    
    public void setLeClient(Client leClient) {
        this.leClient = leClient;
    }
   
    
    public Long getNumero() {
        return numero;
    }
    
    public void setNumero(Long numero) {
        this.numero = numero;
    }
    
    
    public List<Intervention> getLesInterventions() {
        return lesInterventions;
    }

    public void setLesInterventions(List<Intervention> lesInterventions) {
        this.lesInterventions = lesInterventions;
    }
    
    public Date getDateCont() {
        return dateCont;
    }

    public void setDateCont(Date dateCont) {
        this.dateCont = dateCont;
    }
    
    //</editor-fold>
   
}
