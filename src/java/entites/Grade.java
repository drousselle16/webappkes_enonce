
package entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Grade implements Serializable {

    
    //<editor-fold defaultstate="collapsed" desc="Identifiant Clé primaire">
    
    @Id
    private Long    code;
    
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="Attributs informationnels">
    
    private String  libelle;
    private Float   tauxHoraire;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public Float getTauxHoraire() {
        return tauxHoraire;
    }
    
    public void setTauxHoraire(Float tauxHoraire) {
        this.tauxHoraire = tauxHoraire;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    
    
    
    
    
    public Long getCode() {
        return code;
    }
    
    public void setCode(Long code) {
        this.code = code;
    }
    //</editor-fold>
    
}
