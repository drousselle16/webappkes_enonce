
package web.controleurs;

import entites.Client;
import entites.Contrat;
import entites.Technicien;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;


@Named
@RequestScoped

public class ControleurDemo {
  
    private Long    numcont;
    private Long    numtech;
    private Long    numclient;

  
    private Contrat contrat;
    private Client client;
    private Technicien technicien;
    
    public void ecouteurRecherche(){
    
        contrat= Contrat.getLeContrat(numcont);
    
    }
    public void Recherchetech(){
    
        technicien = Technicien.getLeTechnicien(numtech);
    
    }   

    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public Long getNumclient() {
        return numclient;
    }

    public Client getClient() {
        return client;
    }
    
    public Long getNumcont() {
        return numcont;
    }
    public Long getNumtech() {
        return numtech;
    }

    public void setNumcont(Long numcont) {
        this.numcont = numcont;
    }
    public void setNumtech(Long numtech) {
        this.numtech = numtech;
    }    
     public Contrat getContrat() {
        return contrat;
    }
     public Technicien getTechnicien() {
        return technicien;
    }

    public void setTechnicien(Technicien technicien) {
        this.technicien = technicien;
    }
    
    
    //</editor-fold>
      
}
